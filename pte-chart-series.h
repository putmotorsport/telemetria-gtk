#pragma once
#include <gtk/gtk.h>
#include <slope/slope.h>
#include "pte-line-desc.h"

#define PTE_TYPE_CHART_SERIES (pte_chart_series_get_type())

G_BEGIN_DECLS

G_DECLARE_FINAL_TYPE (PteChartSeries, pte_chart_series, PTE, CHART_SERIES, SlopeXySeries)

SlopeItem  *pte_chart_series_new          (PteLineDesc *desc,
                                           guint        limit,
                                           guint        size);

void        pte_chart_series_set_desc     (PteChartSeries  *self,
                                           PteLineDesc     *desc);

void        pte_chart_series_set_size     (PteChartSeries  *self,
                                           guint            size);

void        pte_chart_series_add_point    (PteChartSeries  *self,
                                           gint             x,
                                           gint             y);

void        pte_chart_series_update       (PteChartSeries  *self);

G_END_DECLS
