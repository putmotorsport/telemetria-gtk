#include "pte-chart-series.h"

struct _PteChartSeries
{
  SlopeXySeries   parent_instance;

  guint           limit;
  guint           size;

  gdouble        *x_values;
  gdouble        *y_values;

  gint            count;
};

G_DEFINE_TYPE (PteChartSeries, pte_chart_series, SLOPE_TYPE_XYSERIES)

enum
{
  PROP_NONE,
  PROP_DESC,
  PROP_LIMIT,
  PROP_SIZE,
  PROP_COUNT
};

static GParamSpec *properties[PROP_COUNT];

static void
set_property (GObject      *gobj,
              guint         prop_id,
              const GValue *val,
              GParamSpec   *pspec)
{
  PteChartSeries *self;
  PteLineDesc *desc;

  self = PTE_CHART_SERIES (gobj);

  switch (prop_id)
    {
    case PROP_DESC:
      desc = PTE_LINE_DESC (g_value_get_pointer (val));

      pte_chart_series_set_desc (self, desc);
      break;

    case PROP_LIMIT:
      self->limit = g_value_get_uint (val);
      self->x_values = g_new0 (gdouble, self->limit);
      self->y_values = g_new0 (gdouble, self->limit);
      break;

    case PROP_SIZE:
      self->size = g_value_get_uint (val);
      break;

    default:
      G_OBJECT_CLASS (pte_chart_series_parent_class)
        ->set_property (gobj, prop_id, val, pspec);
    }
}

static void
finalize (GObject *gobj)
{
  g_free (PTE_CHART_SERIES (gobj)->x_values);
  g_free (PTE_CHART_SERIES (gobj)->y_values);

  G_OBJECT_CLASS (pte_chart_series_parent_class)->finalize (gobj);
}

static void
pte_chart_series_init (PteChartSeries *self)
{
  self->count = 0;

  slope_xyseries_set_style (SLOPE_XYSERIES (self), "b-");
}

static void
pte_chart_series_class_init (PteChartSeriesClass *cls)
{
  G_OBJECT_CLASS (cls)->set_property = set_property;
  G_OBJECT_CLASS (cls)->finalize = finalize;

  properties[PROP_NONE] = NULL;

  properties[PROP_DESC]
    = g_param_spec_pointer ("desc", 
                            "line description", 
                            "description of a line",
                            G_PARAM_WRITABLE | G_PARAM_CONSTRUCT);

  properties[PROP_LIMIT]
    = g_param_spec_uint ("limit",
                         "size limit",
                         "maximum available size",
                         1, 1000,
                         1000,
                         G_PARAM_WRITABLE | G_PARAM_CONSTRUCT_ONLY);

  properties[PROP_SIZE]
    = g_param_spec_uint ("size",
                         "series size",
                         "length of chart series",
                         1, 1000,
                         100,
                         G_PARAM_WRITABLE | G_PARAM_CONSTRUCT);

  g_object_class_install_properties (G_OBJECT_CLASS (cls), 
                                     PROP_COUNT, properties);
}

SlopeItem *
pte_chart_series_new (PteLineDesc  *desc, 
                      guint         limit,
                      guint         size)
{
  return g_object_new (PTE_TYPE_CHART_SERIES, 
                       "desc", desc, 
                       "limit", limit,
                       "size", size,
                       NULL);
}

void
pte_chart_series_set_desc (PteChartSeries *self,
                           PteLineDesc    *desc)
{
}

void
pte_chart_series_add_point (PteChartSeries *self,
                            gint            x,
                            gint            y)
{
  gint index = self->count % self->size;

  self->x_values[index] = x;
  self->y_values[index] = y;
  self->count++;
}

void
pte_chart_series_update (PteChartSeries *self)
{
  gint size = MIN(self->count, self->size);

  slope_xyseries_update_data (SLOPE_XYSERIES (self), 
                              self->x_values, 
                              self->y_values, 
                              size);
}
