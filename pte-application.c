#include <json-glib/json-glib.h>

#include "pte-application.h"
#include "pte-chart-grid.h"

struct _PteApplication
{
  GtkApplication  parent_instance;
  GtkWidget      *window;
  JsonParser     *jparser;
};

G_DEFINE_TYPE (PteApplication, pte_application, GTK_TYPE_APPLICATION)

static void 
activate (PteApplication *self, gpointer data)
{
  GtkWidget *grid;
  JsonNode *node;

  printf ("activate: %p\n", self);

  self->window = gtk_application_window_new (GTK_APPLICATION (self));

  gtk_window_set_title (GTK_WINDOW (self->window), "telemetria");
  gtk_window_set_default_size (GTK_WINDOW (self->window), 400, 400);


  grid = pte_chart_grid_new (6);


  json_parser_load_from_file (self->jparser, "charts.json", NULL);
  node = json_parser_get_root (self->jparser);

  if (node)
      pte_chart_grid_setup_json (PTE_CHART_GRID (grid), node);

  gtk_container_add (GTK_CONTAINER (self->window), grid);
  gtk_widget_show_all (self->window);
}

static void
finalize (GObject *gobj)
{
  g_object_unref (PTE_APPLICATION (gobj)->jparser);

  G_OBJECT_CLASS (pte_application_parent_class)->finalize (gobj);
}

static void
pte_application_init (PteApplication *self)
{
  self->jparser = json_parser_new ();

  g_signal_connect (self, "activate", G_CALLBACK (activate), NULL);
}

static void
pte_application_class_init (PteApplicationClass *cls)
{
  G_OBJECT_CLASS (cls)->finalize = finalize;
}

PteApplication *
pte_application_new ()
{
  return g_object_new (PTE_TYPE_APPLICATION, NULL);
}
