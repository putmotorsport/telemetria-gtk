#pragma once
#include <gtk/gtk.h>
#include <json-glib/json-glib.h>
#include <slope/slope.h>

#define PTE_TYPE_CHART (pte_chart_get_type())

G_BEGIN_DECLS

G_DECLARE_FINAL_TYPE (PteChart, pte_chart, PTE, CHART, SlopeView)

GtkWidget    *pte_chart_new           ();

gboolean      pte_chart_setup_json    (PteChart *self,
                                       JsonNode *node);

void          pte_chart_set_value     (PteChart  *self,
                                       gint       id,
                                       gint       value);

void          pte_chart_update        (PteChart  *self);

G_END_DECLS
