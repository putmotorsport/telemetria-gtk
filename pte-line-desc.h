#pragma once
#include <glib-object.h>

#define PTE_TYPE_LINE_DESC (pte_line_desc_get_type ())

G_BEGIN_DECLS

G_DECLARE_FINAL_TYPE (PteLineDesc, pte_line_desc, PTE, LINE_DESC, GObject)

PteLineDesc  *pte_line_desc_new             ();

void          pte_line_desc_set_name        (PteLineDesc *self,
                                             const gchar *name);

void          pte_line_desc_set_color       (PteLineDesc *self,
                                             guint32      rgba);

gboolean      pte_line_desc_set_color_text  (PteLineDesc *self,
                                             const gchar *colname);

void          pte_line_desc_set_id          (PteLineDesc *self,
                                             gint         id);

G_END_DECLS
