#include "pte-line-desc.h"

struct _PteLineDesc
{
  GObject   parent_instance;

  gchar    *name;
  guint32   color;
  gint      id;
};

G_DEFINE_TYPE (PteLineDesc, pte_line_desc, G_TYPE_OBJECT)

static void 
finalize (GObject *gobj)
{
  g_free (PTE_LINE_DESC (gobj)->name);

  G_OBJECT_CLASS (pte_line_desc_parent_class)->finalize (gobj);
}

static void
pte_line_desc_init (PteLineDesc *self)
{
  self->name = g_strdup ("default name");
  self->color = 0;
  self->id = 0;
}

static void
pte_line_desc_class_init (PteLineDescClass *cls)
{
  G_OBJECT_CLASS (cls)->finalize = finalize;
}

PteLineDesc *
pte_line_desc_new ()
{
  return g_object_new (PTE_TYPE_LINE_DESC, NULL);
}

void
pte_line_desc_set_name (PteLineDesc *self,
                        const gchar *name)
{
  if (self->name)
    g_free (self->name);

  self->name = g_strdup (name);
}

void
pte_line_desc_set_color (PteLineDesc *self,
                         guint32      rgba)
{
  self->color = rgba;
}

gboolean
pte_line_desc_set_color_text (PteLineDesc *self,
                              const gchar *colname)
{
  static struct
  {
    const gchar *str;
    guint32 rgba;
  }
  map[] = 
  {
    { "red",        0xff000000 },
    { "green",      0x00ff0000 },
    { "blue",       0x0000ff00 },
  };

  gint i;

  for (i = 0; i < G_N_ELEMENTS (map); i++)
    {
      if (g_ascii_strcasecmp (colname, map[i].str) == 0)
        {
          pte_line_desc_set_color (self, map[i].rgba);
          return TRUE;
        }
    }

  g_error ("invalid rgb color: %s\n", colname);

  return TRUE;
}

void
pte_line_desc_set_id (PteLineDesc  *self,
                      gint          id)
{
  self->id = id;
}
