#include "pte-chart-grid.h"
#include "pte-chart.h"

struct _PteChartGrid
{
  GtkGrid     parent_instance;
  GSList     *chart_list;
  gint        columns;
};

G_DEFINE_TYPE (PteChartGrid, pte_chart_grid, GTK_TYPE_GRID)

enum
{
  PROP_NONE,
  PROP_COLUMNS,
  PROP_COUNT
};

static GParamSpec *properties[PROP_COUNT];

static void
set_property (GObject      *gobj,
              guint         prop_id,
              const GValue *val,
              GParamSpec   *pspec)
{
  gint cols;

  switch (prop_id)
    {
    case PROP_COLUMNS:
      cols = g_value_get_int (val);

      pte_chart_grid_set_columns (PTE_CHART_GRID (gobj), cols);
      break;

    default:
      G_OBJECT_CLASS (pte_chart_grid_parent_class)
        ->set_property (gobj, prop_id, val, pspec);
    }
}

static void
pte_chart_grid_init (PteChartGrid *self)
{
  self->chart_list = NULL;
  self->columns = 2;

  gtk_grid_set_row_homogeneous (GTK_GRID (self), TRUE);
  gtk_grid_set_column_homogeneous (GTK_GRID (self), TRUE);
}

static void
pte_chart_grid_class_init (PteChartGridClass *cls)
{
  G_OBJECT_CLASS (cls)->set_property = set_property;

  properties[PROP_COLUMNS] =
    g_param_spec_int ("columns",
                      "Columns",
                      "number of columns for layout",
                      1, 10, 2,
                      G_PARAM_WRITABLE | G_PARAM_CONSTRUCT);

  g_object_class_install_properties (G_OBJECT_CLASS (cls), 
                                     PROP_COUNT, properties);
}

static void
layout (PteChartGrid *self)
{
  GSList *ptr;
  gint row, col, idx;

  for (idx = 0, ptr = self->chart_list; ptr; ptr = ptr->next, idx++)
  {
    row = idx / self->columns;
    col = idx % self->columns;

    gtk_grid_attach (GTK_GRID (self), ptr->data, col, row, 1, 1);
  }
}

GtkWidget *
pte_chart_grid_new (gint columns)
{
  return g_object_new (PTE_TYPE_CHART_GRID, "columns", columns, NULL);
}

gboolean
pte_chart_grid_setup_json (PteChartGrid *self,
                           JsonNode     *node)
{
  JsonArray *jarr;
  GtkWidget *chart;
  gboolean ok;
  GList *node_ptr;
  GList *node_list = NULL;
  GSList *chart_list = NULL;


  jarr = json_node_get_array (node);

  if (!jarr)
    {
      g_error ("grid json node is not an array!");
      goto failed;
    }

  node_list = json_array_get_elements (jarr);

  for (node_ptr = node_list; node_ptr; node_ptr = node_ptr->next)
    {
      node = node_ptr->data;
      chart = pte_chart_new ();

      g_object_ref (chart);

      chart_list = g_slist_prepend (chart_list, chart);

      ok = pte_chart_setup_json (PTE_CHART (chart), node);

      if (!ok)
        {
          g_error ("setup chart failed!");
          g_slist_free_full (chart_list, g_object_unref);

          goto failed;
        }
    }

  g_list_free (node_list);
  g_slist_free_full (self->chart_list, g_object_unref);

  self->chart_list = chart_list;

  layout (self);

  return TRUE;

failed:
  g_slist_free_full (chart_list, g_object_unref);
  g_list_free (node_list);

  return FALSE;
}

void
pte_chart_grid_set_columns (PteChartGrid *self,
                            gint          columns)
{
  self->columns = columns;

  layout (self);
}
