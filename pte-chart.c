#include "pte-chart.h"
#include "pte-chart-series.h"
#include "pte-line-desc.h"

struct _PteChart
{
  SlopeView     parent_instance;
  GHashTable   *value_map;
  GHashTable   *series_map;
  SlopeFigure  *figure;
  SlopeScale   *scale;
};

G_DEFINE_TYPE (PteChart, pte_chart, SLOPE_TYPE_VIEW)

static void
finalize (GObject *gobj)
{
  g_hash_table_unref (PTE_CHART (gobj)->value_map);
  g_hash_table_unref (PTE_CHART (gobj)->series_map);

  G_OBJECT_CLASS (pte_chart_parent_class)->finalize (gobj);
}

static void
pte_chart_init (PteChart *self)
{
  self->value_map = g_hash_table_new (g_direct_hash, 
                                      g_direct_equal);

  self->series_map = g_hash_table_new_full (g_direct_hash,
                                            g_direct_equal,
                                            NULL,
                                            g_object_unref);

  self->figure = slope_figure_new ();
  self->scale = slope_xyscale_new ();

  slope_view_set_figure (SLOPE_VIEW (self), self->figure);
  slope_figure_add_scale (self->figure, self->scale);
}

static void
pte_chart_class_init (PteChartClass *cls)
{
  G_OBJECT_CLASS (cls)->finalize = finalize;
}

GtkWidget *
pte_chart_new ()
{
  return g_object_new (PTE_TYPE_CHART, NULL);
}

static void
add_series (gpointer data,
            gpointer self)
{
  PteLineDesc *desc;
  SlopeItem *series;


  desc = (PteLineDesc *) data;
  series = pte_chart_series_new (desc, 1000, 100);

  for (gint i = 0; i < 64; i++)
    pte_chart_series_add_point (PTE_CHART_SERIES (series), i, i * i);

  pte_chart_series_update (PTE_CHART_SERIES (series));

  slope_scale_add_item (PTE_CHART (self)->scale, SLOPE_ITEM (series));
}

gboolean
pte_chart_setup_json (PteChart *self,
                      JsonNode *node)
{
  JsonObject *jobj;
  const gchar *title;
  const gchar *line_name;
  const gchar *line_color;
  gint line_id;
  JsonArray *line_arr;
  PteLineDesc *desc;
  GList *lineptr;
  GList *line_node_list = NULL;
  GSList *line_desc_list = NULL;



  jobj = json_node_get_object (node);

  if (!jobj)
    {
      g_error ("json node should be an object!");
      goto failed;
    }

  title = json_object_get_string_member (jobj, "title");

  if (!title)
    {
      g_error ("missing title field!");
      goto failed;
    }

  line_arr = json_object_get_array_member (jobj, "lines");

  if (!line_arr)
    {
      g_error ("missing lines field!");
      goto failed;
    }


  line_node_list = json_array_get_elements (line_arr);

  for (lineptr = line_node_list; lineptr; lineptr = lineptr->next)
    {
      node = lineptr->data;
      jobj = json_node_get_object (node);

      if (!jobj)
        {
          g_error ("invalid line description!");
          goto failed;
        }

      line_name = json_object_get_string_member (jobj, "name");
      line_color = json_object_get_string_member (jobj, "color");
      line_id = json_object_get_int_member (jobj, "id");

      if (!line_name || !line_color || !line_id)
        {
          g_error ("missing line name, color or id!");
          goto failed;
        }

      desc = pte_line_desc_new ();

      pte_line_desc_set_name (desc, line_name);
      pte_line_desc_set_color_text (desc, line_color);
      pte_line_desc_set_id (desc, line_id);

      line_desc_list = g_slist_prepend (line_desc_list, desc);
    }


  g_list_free (line_node_list);



  g_slist_foreach (line_desc_list, add_series, self);
  g_slist_free_full (line_desc_list, g_object_unref);

  return TRUE;

failed:
  g_list_free (line_node_list);
  g_slist_free_full (line_desc_list, g_object_unref);

  return FALSE;
}

void
pte_chart_set_value (PteChart *self,
                     gint id,
                     gint value)
{
  if (g_hash_table_contains (self->series_map, GINT_TO_POINTER (id)))
    {
      g_hash_table_insert (self->value_map, 
                           GINT_TO_POINTER (id), 
                           GINT_TO_POINTER (value));
    }
}

static void
update_series (gpointer key,
               gpointer value,
               gpointer self)
{
  printf ("update %d -> %d :: %p\n", GPOINTER_TO_INT (key), GPOINTER_TO_INT (value), self);
}

void
pte_chart_update (PteChart *self)
{
  g_hash_table_foreach (self->series_map, update_series, self);
}
