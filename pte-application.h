#pragma once
#include <gtk/gtk.h>

#define PTE_TYPE_APPLICATION (pte_application_get_type())

G_BEGIN_DECLS

G_DECLARE_FINAL_TYPE (PteApplication, pte_application, PTE, APPLICATION, GtkApplication)

PteApplication   *pte_application_new   ();

G_END_DECLS
