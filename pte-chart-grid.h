#pragma once
#include <gtk/gtk.h>
#include <json-glib/json-glib.h>

#define PTE_TYPE_CHART_GRID (pte_chart_grid_get_type())

G_BEGIN_DECLS

G_DECLARE_FINAL_TYPE (PteChartGrid, pte_chart_grid, PTE, CHART_GRID, GtkGrid)

GtkWidget    *pte_chart_grid_new          (gint columns);

gboolean      pte_chart_grid_setup_json   (PteChartGrid *self,
                                           JsonNode     *node);

void          pte_chart_grid_set_columns  (PteChartGrid  *self,
                                           gint           columns);

G_END_DECLS
